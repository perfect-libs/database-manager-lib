import { ConnectionKeys } from '../models/essentialTypes';

export default abstract class ConnectionDatabase {

    protected poolDatabase: any;
    protected databaseKeys: ConnectionKeys;

    constructor(keys: ConnectionKeys) {
        this.databaseKeys = keys;
    }

    protected abstract connect(keys: ConnectionKeys): void;
    public abstract query(query: string, data: any[]): Promise<any>;
    public abstract getAll(table: string): Promise<any[]>;
    public abstract get(table: string, id: number | string): Promise<any>;
    public abstract create(table: string, data: any): Promise<any>;
    public abstract delete(table: string, id: number | string): Promise<any>;
    public abstract update(table: string, data: any): Promise<any>;

}