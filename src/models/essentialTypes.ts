export interface ConnectionKeys {
    host: string;
    database: string;
    user: string;
    port: number;
    password: string;
}

export interface ConnectionType extends ConnectionKeys {
    client: string;
}

export interface KeyTables {
    table: string;
    primaryKey: string;
}