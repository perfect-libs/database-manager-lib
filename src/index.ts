import DatabaseClient from './models/user_models/DatabaseClient';
import { setIdTables } from './functions/processData';
import { ConnectionType,KeyTables } from './models/essentialTypes';

export class DatabaseManager {

    private dataBaseClient: DatabaseClient;

    /**
     * @param keys
     * Here you can define your database client.
     * 
     * Using the client key.
     * 
     * By default we use mysql database.
     * 
     * But you can define:
     * 
     * - mysql
     * - postgresql
     * - mongodb
     * 
     * To use some of these database managers.
     */
    constructor(keys: ConnectionType) {
        this.dataBaseClient = new DatabaseClient(keys);
    }

    public setKeyTables(idTables: KeyTables[]): DatabaseClient {
        setIdTables(idTables);
        return this.dataBaseClient;
    }

}

