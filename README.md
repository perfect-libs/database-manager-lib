<h1 align="center">database-manager-lib</h1>

<p>Libreria para gestion CRUD de bases de datos como mysql, mongo y postgres, escrita en typescript.</p>

<div></div>

<hr>
<h2>Autores</h2>

- Jesús Manuel Leiva Bermúdez
- Sergio David Paez Suarez
<hr>
<h2>Instalación</h2>

instalación del paquete ya sea usando `npm` o `yarn`

```bash
npm i database-manager-lib
```
```bash
yarn add database-manager-lib
```
<hr>
<h2>Uso</h2>

```ts
import { DatabaseManager } from 'database-manager-lib';

//Es necesario definir las credenciales de acceso para poder conectar.

let databaseManager: DatabaseManager = new DatabaseManager(keys: {
        client: string,
        host: string,
        database: string,
        user: string,
        port: number,
        password: string
});
```
Luego
```ts
{...}

//Para poder tener acceso al crud, primero es necesario aclarar que tablas existiran, y que identificadores representaran a los registros de la tabla, por el momento solo se puede usar un único identificador por tabla

let databaseClient: DatabaseClient = databaseManager.setKeyTables([
        { ... }, {
                table: 'table-name',
                primaryKey: 'identifier'
        }, { ... }
]);
```
Con esto tendremos acceso a los metodos principales de un crud
```ts
//Todos los metodos reciben un string table que debe existir en los definidos en el paso anterior, de los contrario se generará un error. El objeto data corresponde a un registro en base de datos, debe existir un atributo identificador que debe tener como nombre el definido en setKeyTables en primaryKey para la tabla que correspondada

databaseClient.getAll(table: string);
databaseClient.get(table: string, id: number | string);
databaseClient.create(table: string, data: any);
databaseClient.delete(table: string, id: number | string);
databaseClient.update(table: string, data: any);

```

<hr>
<h2>Licencia</h2>

[GPL-3.0](LICENSE)
